
from TodoItem import TodoItem

class TodoList(list):

    def __init__(self):
        self.list = list()

    def add_new_task(self, id, task, status, date):
        self.list.append(
            TodoItem(
                id = id,
                task = task,
                status = status,
                date = date
            )
        )
