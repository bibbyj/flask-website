
import datetime

def to_web_format(indate):
    '''
    returns the yyyy-mm-dd date format used in webpages
    '''
    return datetime.datetime.strptime(indate, "%d/%m/%Y").strftime("%Y-%m-%d")


def to_db_format(indate):
    '''
    returns the dd/mm/yyyy date format used in the database
    '''
    return datetime.datetime.strptime(indate, "%Y-%m-%d").strftime("%d/%m/%Y")

