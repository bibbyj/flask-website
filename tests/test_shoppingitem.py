
from sys import path
path.append("../")

from ShoppingItem import ShoppingItem
from ShoppingList import hat_stock, hat_price

alonso_hat = ShoppingItem(
    text= "Alonso Hat",
    url= "https://f1store.formula1.com/en/aston-martin/aston-martin-aramco-f1-2024-fernando-alonso-special-edition-barcelona-gp-cap/t-10216347+p-464401233289643+z-9-132337347?_ref=p-TLP:m-GRID:i-r1c1:po-4",
    img= "https://images.footballfanatics.com/aston-martin/aston-martin-aramco-f1-2024-fernando-alonso-special-edition-barcelona-gp-cap_ss5_p-201191527+pv-1+u-wh48usuukxjmr172vdhf+v-ptzkku8jf0x9gkusa4ik.jpg?_hv=2&w=900",
    stock_fn= hat_stock,
    price_fn= hat_price,
)

alonso_hat.update()
alonso_hat.show_info()

TEST_SUCCESS = True

if alonso_hat._stock_fn is None:
    TEST_SUCCESS = False

if alonso_hat._price_fn is None:
    TEST_SUCCESS = False

if (alonso_hat.price == 0) or (alonso_hat.price == None) :
    TEST_SUCCESS = False

print("    Test Passed\n" if TEST_SUCCESS else "    Test Failed\n")