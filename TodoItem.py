
class TodoItem():

    def __init__(self, task, status, date, id=None):
        self.id = id
        self.task = task
        self.status = status
        self.date = date

    def unfurl(self):
        return (self.task, self.status, self.date)
