
from PIL import Image
from PIL import JpegImagePlugin
JpegImagePlugin._getmp = lambda x: None

import base64
import io
import os

import requests
import string
import random

word_site = "https://www.mit.edu/~ecprice/wordlist.10000"
response = requests.get(word_site)
WORDS = response.content.splitlines()

def load_quadrants(files):
    with Image.open(files[0]) as im0:
        im0.load()
    with Image.open(files[1]) as im1:
        im1.load()
    with Image.open(files[2]) as im2:
        im2.load()
    with Image.open(files[3]) as im3:
        im3.load()
    return im0, im1, im2, im3

def quadrant_collage(im1, im2, im3, im4, border_width):
    # define some easy axis access
    x=0; y=1

    width = im1.width + im2.width + (3*border_width)
    height = im1.height + im3.height + (3 * border_width)
    collage = Image.new('RGB', (width,height), color=(255,255,255))

    pos1 = (border_width, border_width)
    pos2 = (pos1[x] + border_width + im1.width, pos1[y])
    pos3 = (pos1[x], pos1[y] + border_width + im1.height)
    pos4 = (pos2[x], pos1[y] + border_width + im1.height)

    collage.paste(im1, pos1)
    collage.paste(im2, pos2)
    collage.paste(im3, pos3)
    collage.paste(im4, pos4)
    return collage

def get_new_images(archive):
    file_list = os.listdir(archive)
    files = random.sample(file_list, 4)
    for idx, file in enumerate(files):
        files[idx] = (archive + file)
    return files

def get_new_collage(files):
    im0, im1, im2, im3 = load_quadrants(files)
    return quadrant_collage(im0, im1, im2, im3, 30)

def encode_image(image):
    data = io.BytesIO()
    image.save(data, "PNG")
    return base64.b64encode(data.getvalue())

def validate_filename(dir, filename):
    if filename+".png" in os.listdir(dir):
        return None
    else:
        return filename

def valid_random_filename(dir):
    filename=None
    while filename == None or filename+".png" in os.listdir(dir):
        random_words = [
            random.choice(WORDS).decode("utf-8"),
            random.choice(WORDS).decode("utf-8")
            ]
        filename = "_".join(random_words) + "_" + random.choice(string.digits)
        print(filename)
    return filename