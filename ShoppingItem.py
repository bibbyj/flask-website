
from Webscraper import Webscraper

class ShoppingItem():
    def __init__(self, text, link, url, img, stock_fn=None, price_fn=None):

        self.text = text
        self.link = link
        self.url = url
        self.img = img
        self._stock_fn = stock_fn
        self._price_fn = price_fn

        self.webscraper = Webscraper(
            url = self.url,
        )

        self.stock = None
        self.price = None

    def get_stock(self):
        if self._stock_fn is None:
            return None
        self.stock = self.webscraper.get(self._stock_fn)
        return self.stock

    def get_price(self):
        if self._price_fn is None:
            return None
        self.price = self.webscraper.get(self._price_fn)
        return self.price

    def update(self):
        self.webscraper.update()
        self.get_price()
        self.get_stock()

    def show_info(self):
        print(
            f"""
    text: {self.text} \n
    link: {self.link} \n
    url: {self.url} \n
    img: {self.img} \n
    stock_fn: {self._stock_fn} \n
    stock: {self.stock} \n
    price_fn: {self._price_fn} \n
    price: {self.price} \n
    webscraper: {self.webscraper} \n
    """
        )