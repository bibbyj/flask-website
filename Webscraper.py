
import requests
from bs4 import BeautifulSoup

class Webscraper():

    # pretend to be an ipad
    HEADERS = {'User-Agent': 'Mozilla/5.0 (iPad; CPU OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148'}

    def __init__(self, url):
        self.url = url
        self.update()


    def update(self):
        self.page = requests.get(self.url, headers=Webscraper.HEADERS)
        self.soup = BeautifulSoup(self.page.text, "lxml")


    def get(self, fn, latest = True):
        if latest:
            self.update()
        return fn(self.soup)
