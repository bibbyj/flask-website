
# doing this up here makes imports pretty easy later
from sys import path
REQUIRED_MOUNT = "../data/"
path.append(REQUIRED_MOUNT)

from datetime import datetime
from flask import (
    Flask,
    render_template,
    request,
    url_for,
    redirect,
)
from Database import get_next_id
from WebFeatures import (
    todo_new_entry,
    todo_new_action,
    get_all_tasks,
    get_static_files,
    update_db_task,
    get_task,
    db_reset,
)
from ShoppingList import SHOPPING_LIST
from urls import job_links

from PIL import Image
from PIL import JpegImagePlugin
JpegImagePlugin._getmp = lambda x: None
from ImageFeatures import (
    get_new_images,
    get_new_collage,
    encode_image,
    valid_random_filename,
    validate_filename,
)

app=Flask(__name__)
DATABASE = REQUIRED_MOUNT + "flask.db"

collage = Image.Image() # >>> global !!

@app.route("/")
def index():
    for item in SHOPPING_LIST:
        item.update()
    return render_template("index.html", job_links=job_links, watchlist=SHOPPING_LIST)


@app.route("/profile")
def profile():
    return render_template("profile.html")


@app.route("/tutor")
def tutor_pdf():
    return render_template("tutor.html")


@app.route("/photo")
def photo():
    global collage

    files = list()
    files = get_new_images(f"{REQUIRED_MOUNT}photos/")
    collage = get_new_collage(files)
    encoded_img_data = encode_image(collage)

    return render_template("photo.html", message="Click to Save!",img_data=encoded_img_data.decode('utf-8'))


# finally using the app.post decorator
@app.post("/photo")
def post_photo():
    global collage
    message = "default"
    export_folder = f"{REQUIRED_MOUNT}exports/"

    # generate a random name, use input name or return None
    if request.form.get("random"):
        filename = valid_random_filename(export_folder)
    else:
        filename = validate_filename(export_folder, request.form.get("filename"))
    # save the picture if valid filename, set return message
    if filename:
        collage.save(f"{export_folder}{filename}.png")
        message = f"Image saved as: {filename}.png"
    else:
        message = f"Image WAS NOT saved!"
    # redisplay current collage image with status message
    encoded_img_data = encode_image(collage)
    return render_template("photo.html", message=message, img_data=encoded_img_data.decode('utf-8'))


@app.route("/todo", methods=['GET', 'POST'])
def todo():
    filter=None
    # this is a great place to use html <form action = /foo>
    # with @app.post(/foo)
    if request.method == "POST":
        if request.form.get("form_id") == "f_new":
            todo_new_entry(DATABASE, request)
        elif request.form.get("form_id") == "f_action":
            todo_new_action(DATABASE, request)
        else:
            print("unknown form found:", request.form)

    # looking to use ? in URL to easily filter table
    filter = request.args.get("f")

    if filter:
        todo_list = get_all_tasks(DATABASE, filter)
    else:
        todo_list = get_all_tasks(DATABASE)

    return render_template(
        "todo_list.html",
        todo_list=todo_list,
        next_id=1 + get_next_id(DATABASE=DATABASE),
        today=datetime.today().strftime('%Y-%m-%d'),
    )


@app.route("/todo/<num>", methods=['GET', 'POST'])
def edit_todo(num):

    if request.method == 'POST':
        update_db_task(DATABASE, request)
        # now that the task has been edited, redirect back to the main list
        return redirect(url_for("todo"))

    item = get_task(DATABASE, num)
    return render_template("todo_item.html", item = item)


@app.route("/weather")
def weather():
    import requests
    import json
    from datetime import datetime
    from apikey import API_KEY

    CITY = "London,GB-SWK"
    url = f'http://api.openweathermap.org/data/2.5/weather?q={CITY}&appid={API_KEY}&units=metric'
    data = requests.get(url).json()
    weather = {
        "Time": datetime.fromtimestamp(int(data["dt"])).strftime('%H:%M:%S'),
        "Date": datetime.fromtimestamp(int(data["dt"])).strftime('%A %d/%m'),
        "Temperature": data["main"]["temp"],
        "Feels Like": data["main"]["feels_like"],
        "Windspeed": data["wind"]["speed"],
        "Conditions": data["weather"][0]["description"],
        # "Location": data["name"],
    }
    try:
        weather["Rain"] = data["rain"]["1h"]
    except KeyError:
        weather["Rain"] = "None"

    return render_template("weather.html", weather=weather)


# @app.route("/reset-database", methods=['GET', 'POST'])
# def database_reset():
#     if request.method == "POST":
#         db_reset(DATABASE)
#     return render_template("reset.html")


if __name__ == "__main__":
    get_static_files(folder=REQUIRED_MOUNT, files=[
        "collage.jpg",
        "tutor_poster.png",
        "icon.png",
        ])

    import argparse
    # create the argument parser object
    parser = argparse.ArgumentParser(description="Run the Flask website")
    # add arguments
    parser.add_argument("-p", "--port", type=int, nargs='?', default=5000, help="select port to run on")
    # inspect command line a gather arguments
    args = parser.parse_args()

    app.run(host="0.0.0.0", debug=True, port=args.port)