
import sqlite3
from sqlite3 import Error
from TodoItem import TodoItem


def create_database_connection(DATABASE):
    conn = None
    try:
        conn = sqlite3.connect(DATABASE)
    except Error as e:
        print(e)
    return conn


def create_empty_table(DATABASE, conn):
    SQL_DROP = """
        DROP TABLE IF EXISTS tasks;
        """
    SQL_CREATE = """
        CREATE TABLE IF NOT EXISTS tasks (
            id integer PRIMARY KEY,
            task text NOT NULL,
            status text NOT NULL,
            date text NOT NULL
            );
        """
    cursor = conn.cursor()
    cursor.execute(SQL_DROP)
    cursor.execute(SQL_CREATE)


def add_demo_tasks(DATABASE, conn):
    from TodoItem import TodoItem

    SQL = """
        INSERT INTO tasks (task, status, date)
        VALUES (?,?,?)
        """

    task1 = TodoItem(
        task = "tutor posters",
        status = "started",
        date = "12/06/2024"
    )
    task2 = TodoItem(
        task = "dockerise Flask apps",
        status = "pending",
        date = "15/06/2024"
    )
    task3 = TodoItem(
        task = "eat lunch",
        status = "pending",
        date = "06/06/2024"
    )

    data = [task1.unfurl(), task2.unfurl(), task3.unfurl()]

    cursor = conn.cursor()
    cursor.executemany(SQL, data)
    conn.commit()
    return cursor.lastrowid


def extract_all_tasks(DATABASE):
    conn = sqlite3.connect(DATABASE)
    cursor = conn.cursor()

    cursor.execute("select * from tasks")
    rows = cursor.fetchall()
    conn.close()
    return rows


def extract_filtered_tasks(DATABASE, filter):
    conn = sqlite3.connect(DATABASE)
    cursor = conn.cursor()

    if filter == "rejected":
        SQL = """
            SELECT * FROM tasks
            WHERE status != 'rejected'
            """
    elif filter == "done":
        SQL = """
            select * from tasks
            where status!='rejected' and status!='done'
            """
    else:
        SQL = "select * from tasks"
        print("unknown filter selected")

    cursor.execute(SQL)
    rows = cursor.fetchall()
    conn.close()
    return rows


def update_task(DATABASE, item):
    SQL = """
        UPDATE tasks
        SET task = ?, status = ?, date = ?
        WHERE id = ?
        """

    data = [item.task, item.status, item.date, item.id]
    conn = create_database_connection(DATABASE=DATABASE)
    cursor = conn.cursor()
    cursor.execute(SQL, data)
    conn.commit()
    conn.close()


def add_task(DATABASE, item):
    SQL = """
        INSERT INTO tasks (task, status, date)
        VALUES (?,?,?)
        """

    data = [item.task, item.status, item.date]
    conn = create_database_connection(DATABASE=DATABASE)
    cursor = conn.cursor()
    cursor.execute(SQL, data)
    conn.commit()
    conn.close()


def remove_task(DATABASE, task_id):
    SQL = """
        DELETE FROM tasks
        WHERE id = ?
        """

    data = [task_id]
    conn = create_database_connection(DATABASE=DATABASE)
    cursor = conn.cursor()
    cursor.execute(SQL, data)
    conn.commit()
    conn.close()


def extract_task(DATABASE, id):
    conn = sqlite3.connect(DATABASE)
    cursor = conn.cursor()

    cursor.execute("SELECT * FROM tasks WHERE id = ?", [id])
    rows = cursor.fetchall()
    conn.close()
    return rows

def set_task_status(DATABASE, id, status):
    SQL = """
        UPDATE tasks
        SET status = ?
        WHERE id = ?
        """

    data = [status, id]
    conn = create_database_connection(DATABASE=DATABASE)
    cursor = conn.cursor()
    cursor.execute(SQL, data)
    conn.commit()
    conn.close()


def get_next_id(DATABASE):
    SQL = """
        select max(id)
        from tasks
        """
    conn = sqlite3.connect(DATABASE)
    cursor = conn.cursor()

    cursor.execute(SQL)
    rows = cursor.fetchall()
    conn.close()
    return rows[0][0]