
import requests
from bs4 import BeautifulSoup

from TodoList import TodoList
from TodoItem import TodoItem
from Database import (
    add_task,
    remove_task,
    set_task_status,
    extract_all_tasks,
    extract_filtered_tasks,
    update_task,
    create_database_connection,
    create_empty_table,
    add_demo_tasks,
)
from DateConversions import to_db_format, to_web_format

import shutil
import os


def todo_new_entry(DATABASE, request):
    item = TodoItem(
        # id = request.form["id"],
        task = request.form["task"],
        status = request.form["status"],
        date = to_db_format(request.form["date"])
    )
    add_task(DATABASE, item)


def todo_new_action(DATABASE, request):
    id = int(request.form.get("index"))

    if request.form.get("action") == "delete":
        remove_task(DATABASE, id)
    else:
        set_task_status(DATABASE, id, request.form.get("action"))


def get_all_tasks(DATABASE, filter=None):
    if filter:
        tasks = extract_filtered_tasks(DATABASE, filter)
    else:
        tasks = extract_all_tasks(DATABASE)
    todo_list = TodoList()

    for task in tasks:
        todo_list.add_new_task(
            task = task[1],
            status = task [2],
            date = task[3],             # rendering date in todo list, use db format
            id = task[0],
        )
    return todo_list


def get_task(DATABASE, num):
    tasks = extract_all_tasks(DATABASE)
    for task in tasks:
        item = TodoItem(
            task = task[1],
            status = task[2],
            date = to_web_format(task[3]),      # rendering in item list with date box needs web format
            id = task[0],
        )
        if item.id == int(num):
            return item
    assert ValueError("Error in Todo List")


def update_db_task(DATABASE, request):
    item = TodoItem(
        id = request.form["id"],
        task = request.form["task"],
        status = request.form["status"],
        date = to_db_format(request.form["date"]),
    )
    update_task(DATABASE, item)


def db_reset(DATABASE):
    conn = create_database_connection(DATABASE=DATABASE)
    create_empty_table(DATABASE=DATABASE, conn=conn)
    # add_demo_tasks(DATABASE=DATABASE, conn=conn)
    conn.close()


def get_static_files(folder, files):
    for file in files:
        if not os.path.isfile(os.path.join("static", file)):
            shutil.copy2(os.path.join(folder, file), os.path.join("static/"))