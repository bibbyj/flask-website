
FROM python

RUN pip install --upgrade pip
RUN pip install --prefer-binary  lxml
RUN pip install Flask bs4 requests pillow

WORKDIR /app
COPY . .
CMD ["python", "app.py"]