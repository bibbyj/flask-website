
from ShoppingItem import ShoppingItem
import urls

SHOPPING_LIST = []

def hat_stock(soup):
        return soup.find("span", class_="stock-availability").find_next(string=True).strip().split("-")[0]

def hat_price(soup):
        return soup.find("span", class_="sr-only").find_next(string=True).strip()[1:]

SHOPPING_LIST.append(
    alonso_hat := ShoppingItem(
        text= urls.hat_info["text"],
        link= urls.hat_info["link"],
        img= urls.hat_info["img"],
        url= urls.hat_info["url"],
        stock_fn= hat_stock,
        price_fn= hat_price,
    )
)

def ram_stock(soup):
    return soup.find("span", class_="small d-inline-block ms-1").find_next(string=True).strip()

def ram_price(soup):
    price_spans = soup.findAll("span", class_="price__amount")
    for span in price_spans:
        if span.get("data-qa") == "price-current":
            return span.find_next(string=True).strip()[1:]
    return "PRICE NOT FOUND"

# SHOPPING_LIST.append(
#     ram1 := ShoppingItem(
#         text= urls.ram1_info["text"],
#         link= urls.ram1_info["link"],
#         img= urls.ram1_info["img"],
#         url= urls.ram1_info["url"],
#         stock_fn=ram_stock,
#         price_fn=ram_price,
#     )
# )

# SHOPPING_LIST.append(
#     ram2 := ShoppingItem(
#         text= urls.ram2_info["text"],
#         link= urls.ram2_info["link"],
#         img= urls.ram2_info["img"],
#         url= urls.ram2_info["url"],
#         stock_fn=ram_stock,
#         price_fn=ram_price,
#     )
# )
